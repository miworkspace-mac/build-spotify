#!/bin/bash -ex

# CONFIG
prefix="Spotify"
suffix=""
munki_package_name="Spotify"
display_name="Spotify"
url="https://download.scdn.co/Spotify.dmg"

# download it (-L: follow redirects)
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36' "${url}"

mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

## Unpack
# /usr/sbin/pkgutil --expand "${mountpoint}/*.pkg" pkg
mkdir -p build-root/Applications
app_in_dmg=$(ls -d ${mountpoint}/*.app)
cp -R $app_in_dmg build-root/Applications
chmod -R a+rx build-root/Applications/*

# Obtain version info
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleVersion" $app_in_dmg/Contents/Info.plist | awk -F'.' '{print $1"."$2"."$3"."$4}'`
#identifier=`/usr/libexec/PlistBuddy -c "Print :CFBundleIdentifier" $app_in_dmg/Contents/Info.plist`
# (cd build-root; pax -rz -f ../pkg/*/Payload)
hdiutil detach "${mountpoint}"

## Create pkg's
/usr/bin/pkgbuild --root build-root/ --ownership recommended --identifier edu.umich.izzy.pkg.$munki_package_name --install-location / --version $version app.pkg

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.pkg ${key_files} --postinstall_script=postinstall.sh | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist

plist=`pwd`/app.plist

# TODO SET THIS and remove the exit -1
# defaults write "${plist}" blocking_applications -array "VirtualBox.app" "/Path/To/Executable" "iTunesHelper"
# defaults write "${plist}" unattended_install -bool YES
# echo 'TODO - configure for unattended install, if it can be'
# exit 42

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.13.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" category -string "${category}"
defaults write "${plist}" version "${version}"
defaults write "${plist}" unattended_install -bool TRUE


# Obtain display name from Izzy and add to plist
display_name=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist
